package jp.bluetree.gov.ncbi.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;

import jp.bluetree.gov.ncbi.Application;
import jp.bluetree.gov.ncbi.model.Publication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class NCBIServiceTest {
  
  @Autowired
  NCBIService ncbiService;
  
  @Test
  public void testService() {
    JsonNode pub = ncbiService.getSummary("9565568");
    Assert.assertEquals("1998 May 8", pub.findValue("pubdate").asText());
    Assert.assertEquals("Simultaneous expression of type 1 and type 2 Lewis blood group antigens by Helicobacter pylori lipopolysaccharides. Molecular mimicry between h. pylori lipopolysaccharides and human gastric epithelial cell surface glycoforms.", pub.findValue("title").asText());
  }

  @Test
  public void testServiceInvalid() {
	  JsonNode pub = ncbiService.getSummary("Z9565568");
    Assert.assertNull(pub.findValue("pubdate"));
    Assert.assertEquals("Invalid uid Z9565568 at position=0", pub.findValue("error").asText());
  }
}