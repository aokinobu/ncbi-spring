package jp.bluetree.gov.ncbi.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import jp.bluetree.gov.ncbi.config.EutilsConfig;
import jp.bluetree.gov.ncbi.model.Publication;

public class NCBIService {

  private static final Log logger = LogFactory.getLog(NCBIService.class);

//  @Autowired
  RestTemplate restTemplate = new RestTemplate();
  
  public JsonNode  getSummary(String id) {
	    logger.debug("summary " + id);
    return restTemplate.getForObject(EutilsConfig.HOSTNAME + EutilsConfig.PATH + "&id=" + id, JsonNode.class);
  }
}
